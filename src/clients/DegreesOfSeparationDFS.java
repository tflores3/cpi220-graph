package clients;

import java.util.ArrayList;

import graph.DepthFirstPaths;
import graph.Graph;
import graph.SymbolGraph;
import io.StdIn;
import io.StdOut;
import util.Stopwatch;

/******************************************************************************
 *  Compilation:  javac DegreesOfSeparationDFS.java
 *  Execution:    java DegreesOfSeparationDFS filename delimiter source
 *  Dependencies: SymbolGraph.java Graph.java DepthFirstPaths.java StdOut.java
 *  Data files:   http://algs4.cs.princeton.edu/41graph/routes.txt
 *                http://algs4.cs.princeton.edu/41graph/movies.txt
 *  
 *  
 *  % java DegreesOfSeparationDFS movies.txt "/" "Bacon, Kevin"
 *  Kidman, Nicole
 *     Bacon, Kevin
 *     Woodsman, The (2004)
 *     Sedgwick, Kyra
 *     Something to Talk About (1995)
 *     Gillan, Lisa Roberts
 *     Runaway Bride (1999)
 *     Schertler, Jean
 *     ... [1782 movies ] (!)
 *     Eskelson, Dana
 *     Interpreter, The (2005)
 *     Silver, Tracey (II)
 *     Copycat (1995)
 *     Chua, Jeni
 *     Metro (1997)
 *     Ejogo, Carmen
 *     Avengers, The (1998)
 *     Atkins, Eileen
 *     Hours, The (2002)
 *     Kidman, Nicole
 *
 ******************************************************************************/

public class DegreesOfSeparationDFS {
    public static void main(String[] args) {
        String filename  = "src/data/movies.txt";
        String delimiter = "/";

        // StdOut.println("Source: " + source);
        SymbolGraph sg = new SymbolGraph(filename, delimiter);
        Graph G = sg.G();
        System.out.println("Type in the name of a node: ");

        while (StdIn.hasNextLine()) 
    	{
    		String source = StdIn.readLine();
    		DepthFirstPaths  bfs;
    		
    		if (!sg.contains(source)) {
    			StdOut.println(source + " not in database.");
    		}
    		else
    		{
	    		int s = sg.index(source);
	    		bfs = new DepthFirstPaths(G, s);
	    		
	            System.out.println("Type in the name of a second node: ");

			    while (!StdIn.isEmpty()) 
			    {
			    	String sink = StdIn.readLine();
		            if (sg.contains(sink)) {
		                int t = sg.index(sink);
		                if (bfs.hasPathTo(t)) {
		                    for (int v : bfs.pathTo(t)) {
		                        StdOut.println("   " + sg.name(v));
		                    }
		                }
		                else {
		                    StdOut.println("Not connected");
		                }
		            }
		            else {
		                StdOut.println("   Not in database.");
		            }
		        }
    		}
            System.out.println("Type in the name of a node: ");

    	}
    }
}
